package server;

import java.io.*;

public class FileOutputManager {
    private File file;
    private FileOutputStream fileStream;

    public FileOutputManager(String fileName) throws IOException {
        this.file = new File(fileName);
        if (!file.exists())
            file.createNewFile();
        fileStream = new FileOutputStream(file, true);
    }

    public static boolean createDirectory(String path){
        File file = new File(path);
        if (!file.exists())
            return file.mkdirs();
        return false;
    }

    public void write(byte[] data) throws IOException {
        fileStream.write(data);
        fileStream.flush();
    }

    public void close() throws IOException {
        fileStream.close();
    }
}
