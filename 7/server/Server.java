package server;

import sbu.cs.client.FileInputManager;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final String PATH = "database";
    public static void main(String[] args){
        FileOutputManager.createDirectory(PATH);
        try {
            ServerSocket serverSocket = new ServerSocket(5500);
            Socket socket = serverSocket.accept();
            serverSocket.close();
            DataInputStream in = new DataInputStream(
                    new BufferedInputStream(socket.getInputStream()));
            String fileName = in.readUTF();
            long size = in.readLong();
            FileOutputManager file
                    = new FileOutputManager(PATH + "/" + fileName);
            byte[] data = new byte[FileInputManager.ONE_MB];
            while (size > 0){
                int len = in.read(data);
                byte[] tmp = new byte[len];
                System.arraycopy(data, 0, tmp, 0, len);
                size -= len;
                file.write(tmp);
            }
            file.close();
            in.close();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
