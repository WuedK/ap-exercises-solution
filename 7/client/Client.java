package client;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String filePath = in.next();
        DataOutputStream out;
        FileInputManager file;
        try {
            file = new FileInputManager(filePath);
            Socket socket = new Socket("127.0.0.1", 5500);
            out = new DataOutputStream(socket.getOutputStream());
            out.writeUTF(file.getName());
            out.writeLong(file.getSize());
            while (file.dataAvailable() > 0)
                out.write(file.readNext());
            out.flush();
            file.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
