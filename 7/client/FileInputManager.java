package client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileInputManager {
    private File file;
    private FileInputStream fileStream;
    public final static int ONE_MB = 1048576;
    byte[] buffer = new byte[ONE_MB];


    public FileInputManager(String filePath) throws FileNotFoundException {
        file = new File(filePath);
        fileStream = new FileInputStream(file);
    }

    public long getSize(){
        return file.length();
    }

    public String getName(){
        return file.getName();
    }

    public int dataAvailable() throws IOException {
        return fileStream.available();
    }

    public byte[] readNext() throws IOException {
        if (fileStream.available() > 0){
            int len = fileStream.read(buffer);
            byte[] tmp = new byte[len];
            System.arraycopy(buffer, 0, tmp, 0, len);
            return tmp;
        }
        return null;
    }

    public void close() throws IOException {
        fileStream.close();
    }
}
