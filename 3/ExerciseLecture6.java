import java.util.*;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for(int i = 0; i < arr.length; i+=2)
            sum += arr[i];
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public static int[] reverseArray(int[] arr) {
        int[] reversed = new int[arr.length];
        for(int i = 0; i < arr.length ;i++)
            reversed[i] = arr[(arr.length - 1) - i];
        return reversed;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        double[][] output = new double[m1.length][m2[0].length];
        if(m1[0].length == m2.length){
            for(int i = 0; i < m1.length; i++)
                for(int j = 0; j < m2[0].length; j++)
                    for(int k = 0; k < m1[0].length; k++)
                        output[i][j] += m1[i][k] * m2[k][j];
        }
        else
            throw new RuntimeException();
        return output;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> output = new ArrayList<>();
        for(String[] row: names) {
            List<String> temp = Arrays.asList(row);
            output.add(temp);
        }
        return output;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        int factor = 2;
        List<Integer> output = new ArrayList<>();
        while(factor <= n){
            while(n % factor == 0){
                n /= factor;
                if(!output.contains(factor))
                    output.add(factor);
            }
            factor++;
        }
        Collections.sort(output);
        return output;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        Pattern patt = Pattern.compile("[\\w]+");
        Matcher m = patt.matcher(line);
        List<String> wordList = new ArrayList<>();
        while (m.find())
            wordList.add(m.group());
        return wordList;
    }

}
