public class TallyCounter implements TallyCounterInterface{

    private int current;

    @Override
    public void count(){
        if(current < 9999)
            current++;
    }

    @Override
    public int getValue() {
        return current;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        if(value < 10000 && value > -1)
            current = value;
        else
            throw new IllegalValueException();
    }

}
