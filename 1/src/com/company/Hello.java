package com.company;

import java.util.Scanner;

public class Hello {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String inp = in.next();
        String pattern = "^.*h.*e.*l.*l.*o.*$";
        System.out.println(inp.matches(pattern) ? "YES" : "NO");
    }
}
