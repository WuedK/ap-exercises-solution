package com.company;

import java.util.Scanner;

public class ExtraPoint {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int x, y, pos = 0, neg = 0;
        for (int i = 0; i < n; i++) {
            x = in.nextInt();
            y = in.nextInt();
            if(x > 0)
                pos++;
            else
                neg++;
        }
        if(pos <= 1 || neg <= 1)
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}
