package com.company;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Discount {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String pattern = in.next();

        // this part is for method 2
        Set<Character> patternSet = new HashSet<>();
        for(int i = 0; i < pattern.length(); i++)
            patternSet.add(pattern.charAt(i));

        for(int i = 0; i < n; i++) {
            String input = in.next();
            System.out.println("method1 : "
                    + (checkPattern1(pattern, input) ? "Yes" : "No"));
            System.out.println("method2 : "
                    + (checkPattern2(patternSet, input) ? "Yes" : "No"));
        }
    }

    static boolean checkPattern1(String pattern, String code) {
        for(int i = 0; i < code.length(); i++){
            boolean found = false;
            for (int j = 0; j < pattern.length(); j++){
                if (code.charAt(i) == pattern.charAt(j)) {
                    found = true;
                    break;
                }
            }
            if (!found)
                return false;
        }

        for(int i = 0; i < pattern.length(); i++){
            boolean found = false;
            for (int j = 0; j < code.length(); j++){
                if (pattern.charAt(i) == code.charAt(j)) {
                    found = true;
                    break;
                }
            }
            if (!found)
                return false;
        }

        return true;
    }

    static boolean checkPattern2(Set<Character> patternSet, String code) {
        Set<Character> inpSet = new HashSet<>();
        for(int j = 0; j < code.length(); j++)
            inpSet.add(code.charAt(j));
        return inpSet.equals(patternSet);
    }

}
