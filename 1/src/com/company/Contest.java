package com.company;

import java.util.Scanner;

public class Contest {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String pattern = "^(1|14|144)+$";
        String inp = in.next();
        System.out.println(inp.matches(pattern) ? "YES" : "NO");
    }
}
