package com.company;

import java.util.Scanner;

public class Teams {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a, b, teams = 0, cities = 3;
        for (int i = 0; i < cities; i++){
            a = in.nextInt();
            b = in.nextInt();
            teams += Math.min(a, b);
        }
        System.out.println(teams);
    }
}
