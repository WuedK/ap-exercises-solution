package com.company;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Names {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(), max1 = 0, max2 = 0, max3 = 0;
        for(int i = 0; i < n; i++) {
            String name = in.next();

            // first method
            int letters = countLetters1(name);
            if (letters > max1)
                max1 = letters;

            // second method
            letters = countLetters2(name);
            if (letters > max2)
                max2 = letters;

            // third method
            letters = countLetters3(name);
            if (letters > max3)
                max3 = letters;
        }
        System.out.println("first method : " + max1 + "\nsecond method : " + max2
                + "\nthird method : " + max3);
    }

    static int countLetters1(String name) {
        Set<Character> charSet = new HashSet<>();
        for(int i = 0; i < name.length(); i++)
            charSet.add(name.charAt(i));
        return charSet.size();
    }

    static int countLetters2(String name) {
        boolean[] lettersSeen = new boolean[26]; // false by default in java
        int counter = 0;
        for (int i = 0; i < name.length(); i++){
            if (!lettersSeen[(int)name.charAt(i) - 'a']) {
                counter++;
                lettersSeen[(int) name.charAt(i) - 'a'] = true;
            }
        }
        return counter;
    }

    static int countLetters3(String name) {
        int counter = 0;
        for (int i = 0; i < 26; i++)
            if (name.contains(String.valueOf((char)(i + (int)'a'))))
                counter++;
        return  counter;
    }
}
