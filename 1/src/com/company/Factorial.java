package com.company;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt() ;
        int fact = 1 , counter = 0 ;
        while (fact <= n) {
            counter++ ;
            fact *= counter ;
        }
        System.out.println(--counter);
    }
}
