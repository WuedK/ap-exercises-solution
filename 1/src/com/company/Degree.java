package com.company;

import java.util.Scanner;

public class Degree {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] degrees = new int[n];
        for (int i = 1; i < n; i++)
            degrees[i] = degrees[i-1] + in.nextInt();
        int a = in.nextInt(), b = in.nextInt();
        System.out.println(degrees[b-1] - degrees[a-1]);
    }
}
