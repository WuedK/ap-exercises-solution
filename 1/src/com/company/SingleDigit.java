package com.company;

import java.util.Scanner;

public class SingleDigit {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String number = in.next();
        System.out.println(toSingleDigit1(Long.parseLong(number))); // first way
        System.out.println(toSingleDigit2(Long.parseLong(number))); // second way
        System.out.println(toSingleDigit3(number)); // third way -> works for any number that has less than (2^64 / 9) digits
    }

    static int toSingleDigit1(long number) {
        if (number == 0)
            return 0;
        else if (number % 9 == 0)
            return 9;
        else
            return (int)(number % 9);
    }

    static int toSingleDigit2(long number) {
        do {
            int sum = 0;
            while(number >= 10) {
                sum += number % 10;
                number /= 10;
            }
            number += sum;
        } while(number >= 10);
        return (int) number;
    }

    static String toSingleDigit3(String number) {
        if (number.length() < 2)
            return number;

        long sum = 0;
        for (int i = 0; i < number.length(); i++)
            sum += (int)number.charAt(i) - (int)'0';
        return toSingleDigit3(String.valueOf(sum));
    }
}
