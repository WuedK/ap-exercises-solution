package semaphore;

import java.util.concurrent.ArrayBlockingQueue;

public class Chef extends Thread {

	private final ArrayBlockingQueue<Boolean> permision = new ArrayBlockingQueue<>(1); 
	
    public Chef(String name) {
        super(name);
    }
    
    public void offer(Boolean bool) {
    	permision.offer(bool);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
            	SourceManager.offer(this);
            	System.out.println(this.getName() + " wants permision, Priority: " + this.getPriority());
            	permision.take();
            	System.out.println(this.getName() + " got permision, Priority: " + this.getPriority());
                Source.getSource();
            	System.out.println(this.getName() + " released, Priority: " + this.getPriority());
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
