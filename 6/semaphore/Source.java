package semaphore;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class Source {
	
	private static Semaphore s = new Semaphore(2);
	public static CountDownLatch latch = new CountDownLatch(50);
	
    public static void getSource() {
        // some process that is limited to 2 persons concurrently
        try {
        	s.acquire();
            Thread.sleep(10);
            s.release();
            SourceManager.release();
            latch.countDown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
