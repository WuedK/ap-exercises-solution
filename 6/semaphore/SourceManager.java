package semaphore;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;

public class SourceManager extends Thread{

	private static Semaphore ss = new Semaphore(2);
	
	private static PriorityBlockingQueue<Chef> pq = new PriorityBlockingQueue<>(5, new Comparator<Chef>() {
		public int compare (Chef chef1, Chef chef2) {
			return Integer.compare(chef1.getPriority(), chef2.getPriority());
		}
	});
	
	public static void offer(Chef chef) {
		pq.offer(chef);
	}
	
	public static void release() {
		ss.release();
	}

	@Override
	public void run() {
		try {
			while(true) {
				ss.acquire();
				Chef chef = pq.take();
				chef.offer(true);
            	System.out.println("From manager: " + chef.getName() + " got permision, Priority: " + chef.getPriority());
			}
		}catch(InterruptedException e) {
			
		}
	}
	
	
}
