package pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
	
	private static BigDecimal result;
	
    public static String calculate(int floatingPoint) {
        MathContext mc = new MathContext(floatingPoint);
        result = BigDecimal.ZERO;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(floatingPoint);
        for (int i = 0; i < floatingPoint; i++)
        	service.submit(new BBPSequanceElementCalculator(i, latch, mc));
        try {
			latch.await();
			service.shutdown();
			result = result.round(mc);
			return result.toString();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
    }
    
    public static synchronized void add(BigDecimal value) {
    	result = result.add(value);
    }

	public static String getResult() {
		return result.toString();
	}
    
}

