package pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.concurrent.CountDownLatch;

public class BBPSequanceElementCalculator extends Thread{

	private int index;
	private CountDownLatch latch;
	private MathContext mc;
	
	public BBPSequanceElementCalculator(int index, CountDownLatch latch, MathContext mc) {
		this.index = index;
		this.latch = latch;
		this.mc = mc;
	}

	@Override
	public void run() {
		BigDecimal value1 = new BigDecimal(4), value2 = new BigDecimal(-2)
				, value3 = new BigDecimal(-1), value4 = new BigDecimal(-1);
		value1 = value1.divide(new BigDecimal(8 * index + 1), mc);
		value2 = value2.divide(new BigDecimal(8 * index + 4), mc);
		value3 = value3.divide(new BigDecimal(8 * index + 5), mc);
		value4 = value4.divide(new BigDecimal(8 * index + 6), mc);
		
		value1 = value1.add(value2);
		value1 = value1.add(value3);
		value1 = value1.add(value4);

		BigDecimal coefficient = new BigDecimal(16);
		coefficient = coefficient.pow(index);
		PICalculator.add(value1.divide(coefficient, mc));
		latch.countDown();
	}
	
	
}
