package Exception;

import java.util.List;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     * @throws NotImplementedCommandException 
     * @throws UnrecognizedCommandException 
     */
    public void readTwitterCommands(List<String> args) throws NotImplementedCommandException, UnrecognizedCommandException {
    	for (String arg : args)
    		if(Util.getNotImplementedCommands().contains(arg))
    			throw new NotImplementedCommandException();
    		else if(!Util.getImplementedCommands().contains(arg))
    			throw new UnrecognizedCommandException();
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     * @throws BadInputException 
     */
    public void read(String...args) throws BadInputException {
    	int counter = 0;
    	for(String arg : args) {
    		if(counter % 2 != 0) {
    			try {
    				Integer.parseInt(arg);
    			}catch(Exception e) {
    				throw new BadInputException();
    			}
    		}
    		counter++;
    	}
    }
}
