package priority;

import java.util.concurrent.CountDownLatch;

public class WhiteThread extends ColorThread {

    public WhiteThread(CountDownLatch latch) {
		super(latch);
	}

	private static final String MESSAGE = "hi back blacks, hi back blues";

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
    	printMessage();
    	latch.countDown();
    }
}
