package priority;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Runner {

	private CountDownLatch latch;
    public static List<Message> messages = new ArrayList<>();

    /**
     * add your codes to this function. this function is the caller function which will be called first.
     * changing other codes in this function is allowed.
     *
     * @param blackCount    number of black threads
     * @param blueCount     number of blue threads
     * @param whiteCount    number of white threads
     */
    public void run(int blackCount, int blueCount, int whiteCount) throws InterruptedException {
        List<ColorThread> colorThreads = new ArrayList<>();
        // your codes here
        ExecutorService service = Executors.newFixedThreadPool(10);
        
        latch = new CountDownLatch(blackCount);
        for (int i = 0; i < blackCount; i++) {
            BlackThread blackThread = new BlackThread(latch);
            colorThreads.add(blackThread);
            service.submit(blackThread);
        }
        latch.await();
        latch = new CountDownLatch(blueCount);
        for (int i = 0; i < blueCount; i++) {
            BlueThread blueThread = new BlueThread(latch);
            colorThreads.add(blueThread);
            service.submit(blueThread);
        }
        latch.await();
        latch = new CountDownLatch(whiteCount);
        for (int i = 0; i < whiteCount; i++) {
            WhiteThread whiteThread = new WhiteThread(latch);
            colorThreads.add(whiteThread);
            service.submit(whiteThread);
        }
        latch.await();
        service.shutdown();
    }

    synchronized public static void addToList(Message message) {
        messages.add(message);
    }

    public List<Message> getMessages() {
        return messages;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

    }
}
