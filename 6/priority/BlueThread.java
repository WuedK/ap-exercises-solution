package priority;

import java.util.concurrent.CountDownLatch;

public class BlueThread extends ColorThread {

    public BlueThread(CountDownLatch latch) {
		super(latch);
	}

	private static final String MESSAGE = "hi back blacks, hi whites";

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
    	printMessage();
    	latch.countDown();
    }
}
