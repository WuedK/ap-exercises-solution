package solutions.ap;

import solutions.ap.squars.*;

public class Machine {
    Square[][] body;
    int size;

    public Machine(int size, int[][] numbers) {
        if (size < 3)
            throw new RuntimeException("invalid size " + size);

        this.size = size;
        body = new Square[size][size];

        Data[] downOutputs = new Data[size];
        generateDownOutputs(downOutputs);

        generateFirstRow(downOutputs, numbers);
        generateMiddleRows(downOutputs, numbers);
        generateLastRow(downOutputs, numbers);
    }

    public String compute(String input){
        ((FirstSquare)body[0][0]).setInput(input);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                //System.out.print(i + " : " + j + " :: ");
                body[i][j].compute();
                //System.out.println();
            }
        }
        return ((LastSquare)body[size - 1][size - 1]).getOutput();
    }

    private void generateLastRow(Data[] upInputs, int[][] numbers) {
        Data rightOut = new Data();
        body[size - 1][0] = new YellowSquare(upInputs[0], rightOut
                , numbers[size - 1][0]);

        for (int i = 1; i < size - 1; i++){
            Data input = rightOut;
            rightOut = new Data();
            body[size - 1][i] = new PinkSquare(input, upInputs[i], rightOut
                    , numbers[size - 1][i]);
        }

        body[size - 1][size - 1] = new LastSquare(rightOut, upInputs[size - 1], new Data()
                , numbers[size - 1][size - 1]);
    }

    private void generateMiddleRows(Data[] upInputs, int[][] numbers) {
        Data[] downOutputs = new Data[size];

        for (int i = 1; i < size - 1; i++){
            generateDownOutputs(downOutputs);
            Data rightOut = new Data();
            body[i][0] = new GreenSquare(upInputs[0], rightOut, downOutputs[0]
                    , numbers[i][0]);

            for (int j = 1; j < size - 1; j++){
                Data input = rightOut;
                rightOut = new Data();
                body[i][j] = new BlueSquare(input, upInputs[j], rightOut
                        , downOutputs[j], numbers[i][j]);
            }

            body[i][size - 1] = new PinkSquare(rightOut, upInputs[size - 1]
                    , downOutputs[size - 1], numbers[i][size - 1]);

            System.arraycopy(downOutputs, 0, upInputs, 0, size);
        }
    }

    private void generateFirstRow(Data[] downOutputs, int[][] numbers) {
        Data rightOut = new Data();
        body[0][0] = new FirstSquare(new Data(), rightOut, downOutputs[0]
                , numbers[0][0]);

        for (int i = 1; i < size - 1; i++){
            Data input = rightOut;
            rightOut = new Data();
            body[0][i] = new GreenSquare(input, rightOut, downOutputs[i]
                    , numbers[0][i]);
        }

        body[0][size - 1] = new YellowSquare(rightOut, downOutputs[size - 1]
                , numbers[0][size - 1]);
    }

    private void generateDownOutputs(Data[] downOutputs){
        for(int i = 0; i < size; i++)
            downOutputs[i] = new Data();
    }
}
//setInput
//compute first row
// compute first column
// compute middle rows
// compute middle columns
// compute last row
// compute last column
//getout
/*
"" "" ""
"" "" ""
"" "" ""
 */

