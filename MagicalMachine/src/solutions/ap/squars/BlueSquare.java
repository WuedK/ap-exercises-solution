package solutions.ap.squars;

import solutions.ap.BlackFunctions;
import solutions.ap.Data;


public class BlueSquare implements Square {
    protected Data leftInput;
    protected Data upInput;
    protected Data rightOutput;
    protected Data downOutput;
    protected final int number;

    public BlueSquare(Data leftInput, Data upInput, Data rightOutput
            , Data downOutput, int number) {
        this.leftInput = leftInput;
        this.upInput = upInput;
        this.rightOutput = rightOutput;
        this.downOutput = downOutput;
        this.number = number;
    }

    @Override
    public void compute() {
        //System.out.print("Blue : Lin = " + leftInput.getData());
        //System.out.print(" Uin = " + upInput.getData());

        rightOutput.setData(BlackFunctions.compute(number, leftInput.getData()));
        downOutput.setData(BlackFunctions.compute(number, upInput.getData()));

        //System.out.print(" Rout = " + rightOutput.getData());
        //System.out.print(" Dout = " + downOutput.getData());
    }
}
