package solutions.ap.squars;

import solutions.ap.BlackFunctions;
import solutions.ap.Data;


public class GreenSquare implements Square {
    protected Data input;
    protected Data rightOutput;
    protected Data downOutput;
    protected final int number;

    public GreenSquare(Data input, Data rightOutput, Data downOutput, int number) {
        this.input = input;
        this.rightOutput = rightOutput;
        this.downOutput = downOutput;
        this.number = number;
    }

    @Override
    public void compute() {
        //System.out.print("Green : input = " + input.getData());

        rightOutput.setData(BlackFunctions.compute(number, input.getData()));
        downOutput.setData(rightOutput.getData());

        //System.out.print(" Rout = " + rightOutput.getData());
        //System.out.print(" Dout = " + downOutput.getData());
    }
}
