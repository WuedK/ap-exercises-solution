package solutions.ap.squars;

import solutions.ap.BlackFunctions;
import solutions.ap.Data;

public class YellowSquare implements Square {
    protected Data input;
    protected Data output;
    protected final int number;

    public YellowSquare(Data input, Data output, int number) {
        this.input = input;
        this.output = output;
        this.number = number;
    }

    @Override
    public void compute() {
        //System.out.print("Yellow : in = " + input.getData());

        output.setData(BlackFunctions.compute(number, input.getData()));

        //System.out.print(" out = " + output.getData());
    }
}
