package solutions.ap.squars;

import solutions.ap.Data;
import solutions.ap.WhiteFunctions;

public class PinkSquare implements Square {
    protected Data leftInput;
    protected Data upInput;
    protected Data output;
    protected final int number;

    public PinkSquare(Data leftInput, Data upInput, Data output, int number) {
        this.leftInput = leftInput;
        this.upInput = upInput;
        this.output = output;
        this.number = number;
    }

    @Override
    public void compute() {
        //System.out.print("Pink : Lin = " + leftInput.getData());
        //System.out.print(" Uin = " + upInput.getData());

        output.setData(WhiteFunctions.compute(number, leftInput.getData()
                , upInput.getData()));

        //System.out.print(" out = " + output.getData());
    }
}
