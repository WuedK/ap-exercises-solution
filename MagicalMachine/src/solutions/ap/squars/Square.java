package solutions.ap.squars;

public interface Square {
    void compute();
}
