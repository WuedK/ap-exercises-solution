package solutions.ap.squars;

import solutions.ap.Data;

public class FirstSquare extends GreenSquare {
    public FirstSquare(Data input, Data rightOutput, Data downOutput, int number) {
        super(input, rightOutput, downOutput, number);
    }

    public void setInput(String input){
        this.input.setData(input);
    }
}
