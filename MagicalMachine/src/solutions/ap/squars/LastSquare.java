package solutions.ap.squars;

import solutions.ap.Data;

public class LastSquare extends PinkSquare{
    public LastSquare(Data leftInput, Data upInput, Data output, int number) {
        super(leftInput, upInput, output, number);
    }

    public String getOutput(){
        return this.output.getData();
    }
}




