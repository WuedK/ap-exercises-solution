package solutions.ap;

public class WhiteFunctions {
    public static String compute(int number, String input1, String input2){
        switch (number) {
            case 1:
                return mix(input1, input2);
            case 2:
                return reverseAppend(input1, input2);
            case 3:
                return reverseMix(input1, input2);
            case 4:
                return evenOrOdd(input1, input2);
            case 5:
                return add(input1, input2);
            default:
                throw new RuntimeException("not a valid number.");
        }
    }

    private static String add(String input1, String input2) {
        StringBuilder res = new StringBuilder();
        int min = Math.min(input1.length(), input2.length());
        for(int i = 0; i < min; i++)
            res.append((char)((input1.charAt(i) + input2.charAt(i) - 2 * 'a') % 26
                    + 'a'));
        res.append(input1.substring(min)).append(input2.substring(min));
        return res.toString();
    }

    private static String evenOrOdd(String input1, String input2) {
        return (input1.length() % 2 == 0) ? input1 : input2;
    }

    private static String reverseMix(String input1, String input2) {
        return mix(input1, new StringBuilder(input2).reverse().toString());
    }

    private static String reverseAppend(String input1, String input2) {
        return input1 + new StringBuilder(input2).reverse().toString();
    }

    private static String mix(String input1, String input2) {
        StringBuilder res = new StringBuilder();
        int min = Math.min(input1.length(), input2.length());
        for(int i = 0; i < 2 * min; i++)
            res.append((i % 2 == 0) ? input1.charAt(i/2) : input2.charAt(i/2));
        res.append(input1.substring(min)).append(input2.substring(min));
        return res.toString();
    }
}
