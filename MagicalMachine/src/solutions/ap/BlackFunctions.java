package solutions.ap;

public class BlackFunctions {
    public static String compute(int number, String input){
        switch (number) {
            case 1:
                return reverse(input);
            case 2:
                return twice(input);
            case 3:
                return duplicate(input);
            case 4:
                return rightShift(input);
            case 5:
                return replace(input);
            default:
                throw new RuntimeException("not a valid number.");
        }
    }

    private static String replace(String input) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < input.length(); i++)
            res.append((char)(25 - input.charAt(i) + 2 * 'a'));
        return res.toString();
    }

    private static String rightShift(String input) {
        return input.charAt(input.length() - 1)
                + input.substring(0,input.length() - 1);
    }

    private static String duplicate(String input) {
        return input + input;
    }

    private static String twice(String input) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < input.length(); i++)
            res.append(input.charAt(i)).append(input.charAt(i));
        return res.toString();
    }

    private static String reverse(String input) {
        return new StringBuilder(input).reverse().toString();
    }

}
