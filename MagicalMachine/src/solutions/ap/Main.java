package solutions.ap;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        for (int i = 1; i <= 5; i++)
//            System.out.println(BlackFunctions.compute(i, "amir"));
//		  System.out.println();
//        for (int i = 1; i <= 5; i++)
//            System.out.println(WhiteFunctions.compute(i, "amir", "pegah"));
        Scanner in = new Scanner(System.in);
        final String expected = "vemehewewjijejzjznenancncryrgrlrljjjpjljldedvdtdtmomimumusdsssosodldcdzdzmdmhmvmzizikizizxzxhxzxzpzptpzpzvzvdvzvzrzrirzrzizimizizvzvkvzvzkzktkzkzqzqwqzqztztotzt";
        int size = in.nextInt();
        int[][] arr = new int[size][size];
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                arr[i][j] = in.nextInt();
        String input = in.next();
        Machine m = new Machine(size, arr);
        String res = m.compute(input);
        System.out.println(res);
        System.out.println(res.equals(expected));
    }
}


/*
4
1 2 3 4
5 1 2 3
4 5 1 2
3 4 5 1
ab

zbyazaybbbbababyabayabazbabzbb

8
2 5 5 4 2 1 5 5
2 1 2 4 4 1 5 4
4 4 1 1 1 5 1 4
4 1 4 4 1 4 5 1
1 1 1 5 1 4 4 5
4 4 5 4 5 1 5 5
1 4 4 4 1 1 1 4
4 5 4 5 5 1 4 4
qmiqwnhwnrckeirepjgv

vemehewewjijejzjznenancncryrgrlrljjjpjljldedvdtdtmomimumusdsssosodldcdzdzmdmhmvmzizikizizxzxhxzxzpzptpzpzvzvdvzvzrzrirzrzizimizizvzvkvzvzkzktkzkzqzqwqzqztztotzt
*/