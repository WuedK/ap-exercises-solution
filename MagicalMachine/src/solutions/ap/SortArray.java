package solutions.ap;

import java.util.Arrays;

public class SortArray {

	 /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for (int i = 0; i < size-1; i++) {
        	int min = i;
        	for (int j = i+1; j < size; j++)
        		if (arr[j] < arr[min])
        			min = j;
        	int temp = arr[i];
        	arr[i] = arr[min];
        	arr[min] = temp;
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i = 1; i < size; i++) {
        	int num = arr[i], j;
        	for (j = i-1; j >= 0 && arr[j] > num; j--) 
        		arr[j+1] = arr[j];
        	arr[j+1] = num;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        sort(arr, 0, size - 1);
        return arr;
    }
    
    private void sort(int arr[], int left, int right) {
    	if (left < right) {
    		int middle = left + (right - left) / 2;
    		sort(arr, left, middle);
    		sort(arr, middle + 1, right);
    		merge(arr, left, middle, right);
    	}
    }
    
    private void merge(int[] arr, int left, int middle, int right) {
    	int sizeLeft = middle - left + 1;
    	int sizeRight = right - middle;
    	int[] leftArray = new int[sizeLeft];
    	int[] rightArray = new int[sizeRight];
    	leftArray = Arrays.copyOfRange(arr, left, sizeLeft + left);
    	rightArray = Arrays.copyOfRange(arr, middle + 1, sizeRight + middle + 1);
    	
    	int i = 0, j = 0, k = left;
    	while (i < sizeLeft && j < sizeRight) {
    		if (leftArray[i] < rightArray[j]) {
    			arr[k] = leftArray[i];
    			i++;
    		}else {
    			arr[k] = rightArray[j];
    			j++;
    		}
    		k++;
    	}
    	
    	while(i < sizeLeft) {
    		arr[k] = leftArray[i];
    		i++;
    		k++;
    	}
    	
    	while(j < sizeRight) {
    		arr[k] = rightArray[j];
    		j++;
    		k++;
    	}
	}

	/**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
    	int left = 0, right = arr.length;
    	while (left <= right) {
    		int middle = left + (right - left) / 2;
    		if (arr[middle] == value)
    			return middle;
    		if (arr[middle] < value)
    			left = middle + 1;
    		else
    			right = middle - 1;
    	}
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
    	int size = arr.length;
    	if (size == 1)
    		return arr[0] == value ? 0 : -1;
    	
    	if (size > 1) {
    		int middle = size / 2;
    		if (arr[middle] == value)
    			return middle;
    		if (arr[middle] < value) {
    			int ans = binarySearchRecursive(Arrays.copyOfRange(arr, middle , arr.length), value);
    			return ans != -1 ? middle + ans : -1;
    		}else
    			return binarySearchRecursive(Arrays.copyOfRange(arr, 0, middle), value);
    	}
        return -1;
    }
}
