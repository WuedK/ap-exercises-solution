package solutions.ap;

import java.util.*;

public class Game {

    static LinkedList<Table> turns = new LinkedList<>();
    static Player player = new Player();
    static Scanner in = new Scanner(System.in);
    static Random rand = new Random();
    static boolean win = false;

    static final int TURNS_MAX_SIZE = 10;
    static final int TABLE_MAX_SIZE = 11;
    static final int TABLE_MIN_SIZE = 2;

    public static void main(String[] args) {
        boolean exit = false;
        do {
            clearScreen();
            MainMenuValues mainMenuResult = mainMenu();
            switch (mainMenuResult) {
                case start:
                    startGame();
                    break;
                case exit:
                    exit = true;
            }
        } while (!exit);
    }

    static char getInput(){
        return in.next().charAt(0);
    }

    static MainMenuValues mainMenu(){
        System.out.println("welcome to 2048!");
        char query;
        do {
            System.out.println("Please enter the row number : ");
            System.out.println("1_ start");
            System.out.println("2_ exit");
            query = getInput();
            clearScreen();
        } while (query != '1' && query != '2');

        return query == '1' ? MainMenuValues.start : MainMenuValues.exit;
    }

    static void setPlayer(){
        System.out.println("Enter your name : ");
        player.name = in.next();
        System.out.println("Enter table size : ");
        int size = in.nextInt();
        while (size > TABLE_MAX_SIZE || size < TABLE_MIN_SIZE){
            System.out.println("Table size must be between 2 and 11." +
                    "\nplease Enter table size again");
            size = in.nextInt();
        }
        win = false;
        player.score = 0;
        turns.clear();
        generateTable(size);
        clearScreen();
    }

    static void generateTable(int size) {
        player.table = new Table();
        player.table.size = size;
        player.table.values = new int[player.table.size][player.table.size];
        player.table.zeros = player.table.size * player.table.size;
        insertRandomNum(2);
    }

    static void insertRandomNum(int times) {
        if (times <= 0 || player.table.zeros == 0)
            return;
        else if (times > player.table.zeros)
            times = player.table.zeros;

        for (int i = 0; i < times; i++) {
            int pos = rand.nextInt(player.table.zeros);
            int num = Math.abs(rand.nextInt()) % 10 >= 8 ? 4 : 2;
            boolean found = false;

            for (int j = 0; j < player.table.size; j++) {
                for (int k = 0; k < player.table.size; k++){
                    if (player.table.values[j][k] == 0){
                        pos--;
                        if (pos == -1){
                            player.table.values[j][k] = num;
                            player.table.zeros--;
                            found = true;
                            break;
                        }
                    }
                }
                if (found)
                    break;
            }
        }
    }

    static void startGame(){
        setPlayer();
        boolean exited = false;
        do {
            printTable();
            char buttonClicked = getInput();
            switch (buttonClicked){
                case 'a':
                    goLeft();
                    break;
                case 's':
                    goDown();
                    break;
                case 'd':
                    goRight();
                    break;
                case 'w':
                    goUp();
                    break;
                case 'u':
                    undo();
                    break;
                case 'e':
                    exited = true;
                    break;
            }
        } while(!checkOver() && !exited);

        if (!exited) {
            printTable();
            System.out.println("\n###################");
            System.out.println((win ? "You Won!" : "Game Over!") +
                    "\nPress Enter to continue");
            in.nextLine();
            in.nextLine();
        }

    }

    static boolean checkOver() {

        boolean over = true;

        if (player.table.zeros != 0)
            over = false;

        for (int row = 0; row < player.table.size; row++)
            for (int col = 0; col < player.table.size - 1; col++) {
                if (player.table.values[row][col]
                        == player.table.values[row][col + 1])
                    over = false;
                if (player.table.values[row][col] == 2048
                        || player.table.values[row][col + 1] == 2048){
                    win = true;
                    return true;
                }
            }

        if (!over)
            return false;

        for (int column = 0; column < player.table.size; column++)
            for (int row = 0; row < player.table.size - 1; row++)
                if (player.table.values[row][column]
                        == player.table.values[row + 1][column])
                    return false;


        return true;
    }

    static void undo() {
        if (!turns.isEmpty())
            player.table = turns.removeLast();
    }

    static void goLeft() {
        Table tmp = getBackUp();
        moveLeft();
        mergeLeft();
        moveLeft();
        if (!Arrays.deepEquals(tmp.values, player.table.values)) {
            if (turns.size() == TURNS_MAX_SIZE)
                turns.removeFirst();
            turns.addLast(tmp);
            insertRandomNum(1);
        }
    }

    static void mergeLeft() {
        for (int row = 0; row < player.table.size; row++) {
            for (int col = 0; col < player.table.size - 1; col++) {
                if (player.table.values[row][col + 1] != 0 &&
                        player.table.values[row][col]
                                == player.table.values[row][col + 1]) {
                    player.table.values[row][col] *= 2;
                    player.score += player.table.values[row][col];
                    player.table.values[row][col + 1] = 0;
                    player.table.zeros++;
                    col++;
                }
            }
        }
    }

    static void moveLeft() {
        for (int row = 0; row < player.table.size; row++) {
            Queue<Integer> nonZeros = new LinkedList<>();
            for (int col = 0; col < player.table.size; col++)
                if (player.table.values[row][col] != 0)
                    nonZeros.add(player.table.values[row][col]);
            for (int col = 0; col < player.table.size; col++){
                if (nonZeros.isEmpty())
                    player.table.values[row][col] = 0;
                else
                    player.table.values[row][col] = nonZeros.poll();
            }
        }

    }

    static void goRight() {
        Table tmp = getBackUp();
        moveRight();
        mergeRight();
        moveRight();
        if (!Arrays.deepEquals(tmp.values, player.table.values)) {
            if (turns.size() == TURNS_MAX_SIZE)
                turns.removeFirst();
            turns.addLast(tmp);
            insertRandomNum(1);
        }
    }

    static void mergeRight() {
        for (int row = 0; row < player.table.size; row++) {
            for (int col = player.table.size - 1; col > 0; col--) {
                if (player.table.values[row][col - 1] != 0 &&
                        player.table.values[row][col]
                                == player.table.values[row][col - 1]) {
                    player.table.values[row][col] *= 2;
                    player.score += player.table.values[row][col];
                    player.table.values[row][col - 1] = 0;
                    player.table.zeros++;
                    col--;
                }
            }
        }
    }

    static void moveRight() {
        for (int row = 0; row < player.table.size; row++) {
            Queue<Integer> nonZeros = new LinkedList<>();
            for (int col = player.table.size - 1; col >= 0; col--)
                if (player.table.values[row][col] != 0)
                    nonZeros.add(player.table.values[row][col]);
            for (int col = player.table.size - 1; col >= 0; col--){
                if (nonZeros.isEmpty())
                    player.table.values[row][col] = 0;
                else
                    player.table.values[row][col] = nonZeros.poll();
            }
        }

    }

    static void goUp() {
        Table tmp = getBackUp();
        moveUp();
        mergeUp();
        moveUp();
        if (!Arrays.deepEquals(tmp.values, player.table.values)) {
            if (turns.size() == TURNS_MAX_SIZE)
                turns.removeFirst();
            turns.addLast(tmp);
            insertRandomNum(1);
        }
    }

    static void mergeUp() {
        for (int col = 0; col < player.table.size; col++) {
            for (int row = 0; row < player.table.size - 1; row++) {
                if (player.table.values[row + 1][col] != 0 &&
                        player.table.values[row][col]
                                == player.table.values[row + 1][col]) {
                    player.table.values[row][col] *= 2;
                    player.score += player.table.values[row][col];
                    player.table.values[row + 1][col] = 0;
                    player.table.zeros++;
                    row++;
                }
            }
        }
    }

    static void moveUp() {
        for (int col = 0; col < player.table.size; col++) {
            Queue<Integer> nonZeros = new LinkedList<>();
            for (int row = 0; row < player.table.size; row++)
                if (player.table.values[row][col] != 0)
                    nonZeros.add(player.table.values[row][col]);
            for (int row = 0; row < player.table.size; row++){
                if (nonZeros.isEmpty())
                    player.table.values[row][col] = 0;
                else
                    player.table.values[row][col] = nonZeros.poll();
            }
        }
    }

    static void goDown() {
        Table tmp = getBackUp();
        moveDown();
        mergeDown();
        moveDown();
        if (!Arrays.deepEquals(tmp.values, player.table.values)) {
            if (turns.size() == TURNS_MAX_SIZE)
                turns.removeFirst();
            turns.addLast(tmp);
            insertRandomNum(1);
        }
    }

    static Table getBackUp() {
        Table backUp = new Table();
        backUp.size = player.table.size;
        backUp.values = new int[backUp.size][backUp.size];
        backUp.zeros = player.table.zeros;

        for(int i = 0; i < player.table.size; i++)
            System.arraycopy(player.table.values[i], 0
                    , backUp.values[i], 0, player.table.size);

        return backUp;
    }

    static void mergeDown() {
        for (int col = 0; col < player.table.size; col++) {
            for (int row = player.table.size - 1; row > 0 ; row--) {
                if (player.table.values[row - 1][col] != 0 &&
                        player.table.values[row][col]
                                == player.table.values[row - 1][col]) {
                    player.table.values[row][col] *= 2;
                    player.score += player.table.values[row][col];
                    player.table.values[row - 1][col] = 0;
                    player.table.zeros++;
                    row--;
                }
            }
        }
    }

    static void moveDown() {
        for (int col = 0; col < player.table.size; col++) {
            Queue<Integer> nonZeros = new LinkedList<>();
            for (int row = player.table.size - 1; row >= 0 ; row--)
                if (player.table.values[row][col] != 0)
                    nonZeros.add(player.table.values[row][col]);
            for (int row = player.table.size - 1; row >= 0 ; row--){
                if (nonZeros.isEmpty())
                    player.table.values[row][col] = 0;
                else
                    player.table.values[row][col] = nonZeros.poll();
            }
        }

    }

    static void printTable() {
        clearScreen();
        System.out.println(player.name + " : " + player.score);
        System.out.println();
        for(int i = 0; i < player.table.size; i++)
            System.out.print("+------+");
        System.out.println();

        for (int i = 0; i < player.table.size; i++){
            for (int j = 0; j < player.table.size; j++)
                System.out.printf("| %4d |", player.table.values[i][j]);
            System.out.println();
            for(int j = 0; j < player.table.size; j++)
                System.out.print("+------+");
            System.out.println();

        }

        System.out.println();
        System.out.println("w : UP   a : LEFT   s : DOWN   " +
                "d : RIGHT   u : UNDO   e : EXIT");

    }

    static void clearScreen() {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

}

class Table{
    public int[][] values;
    public int zeros;
    public int size;
}

class Player{
    public String name;
    public int score;
    public Table table;
}

enum MainMenuValues{
    start, exit
}
