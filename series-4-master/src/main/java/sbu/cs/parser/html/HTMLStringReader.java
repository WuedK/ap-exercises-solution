package sbu.cs.parser.html;

public class HTMLStringReader {
    public final static char TAG_OPEN = '<';
    public final static char TAG_CLOSE = '>';
    public final static char SLASH = '/';

    private String document;
    private int index = 0;

    public HTMLStringReader(String document) {
        this.document = document;
    }

    public boolean isUpcomingTag(){
        return document.charAt(getUpcomingWhiteSpaces()) == TAG_OPEN;
    }

    public String readTag(){
        String tmpDoc = document.substring(index);
        index += tmpDoc.indexOf(TAG_CLOSE) + 1;
        return tmpDoc.substring(tmpDoc.indexOf(TAG_OPEN)
                , tmpDoc.indexOf(TAG_CLOSE) + 1);
    }

    public String readString(){
        StringBuilder text = new StringBuilder();
        while (document.charAt(index) != TAG_OPEN){
            if (Character.isWhitespace(document.charAt(index))){
                text.append(" ");
                skipWhiteSpaces();
            }
            else {
                text.append(document.charAt(index));
                index++;
            }
        }
        return text.toString().trim();
    }

    private void skipWhiteSpaces() {
        while (Character.isWhitespace(document.charAt(index)))
            index++;
    }

    private int getUpcomingWhiteSpaces() {
        int tmpIndex = index;
        while(Character.isWhitespace(document.charAt(tmpIndex)))
            tmpIndex++;
        return tmpIndex;
    }


    // tag/ string / tag or string?
}
