package sbu.cs.parser.html;

public class TagCanNotHaveChildren extends RuntimeException {
    public TagCanNotHaveChildren(String error) {
        super(error);
    }
}
