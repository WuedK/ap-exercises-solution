package sbu.cs.parser.html;

import sbu.cs.parser.helpers.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {

    private String name;
    private HashMap<String, String> attributes = new HashMap<>();
    private ArrayList<Pair<Integer, Node>> children = new ArrayList<>();
    private ArrayList<Pair<Integer, String>> texts = new ArrayList<>();
    private Node parent;
    private boolean isSingle = false;

    public Node(String name) {
        this.name = name;
    }

    public Node(String name, boolean isSingle) {
        this.name = name;
        this.isSingle = isSingle;
    }

    public String getName() {
        return name;
    }

    public Node getParent() {
        return parent;
    }

    public boolean isSingle() {
        return isSingle;
    }


    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void addChild(Node child){
        if (isSingle)
            throw new TagCanNotHaveChildren("this tag is single.");
        if (child.getParent() != null)
            throw new TagCanNotHaveChildren("the child has another parent.");
        child.setParent(this);
        children.add(new Pair<>(children.size() + texts.size(), child));
    }

    public void addText(String text){
        if (isSingle)
            throw new TagCanNotHaveChildren("this tag is single.");
        texts.add(new Pair<>(children.size() + texts.size(), text));
    }

    public void addAttribute(String key, String value){
        attributes.put(key, value);
    }

    /*
    * this function will return all that exists inside a tag
    * for example for <html style="gyhdjksffdkjsfsdkjhg" src="kajhgfs"><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {
        StringBuilder str = new StringBuilder();
        for (int tItt = 0, cItt = 0; tItt + cItt < children.size() + texts.size();){
            if (tItt != texts.size()
                    && tItt + cItt == texts.get(tItt).getFirst()) {
                str.append(texts.get(tItt).getSecond());
                tItt++;
            }
            else{
                str.append(children.get(cItt).getSecond());
                cItt++;
            }

        }
        String res = str.toString();
        return res.isBlank() ? null : res;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("<");
        if (isSingle)
            str.append("/");
        str.append(name);
        for (Map.Entry<String, String> entry : attributes.entrySet())
            str.append(" " + entry.getKey() + "=\"" + entry.getValue() + "\"");
        str.append(">");
        if (isSingle)
            return str.toString();
        str.append(getStringInside());
        str.append("</" + name + ">");
        return str.toString();
    }

    /*
    *   return n th child starting from 0
     */
    @Override
    public Node getChild(int number) {
        return children.get(number).getSecond();
    }

    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        return attributes.get(key);
    }
}
