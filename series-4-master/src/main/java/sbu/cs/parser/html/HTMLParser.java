package sbu.cs.parser.html;

public class HTMLParser {

    /*
    * this function will get a String and returns a Dom object
     */
    public static Node parse(String document) {
        HTMLStringReader reader = new HTMLStringReader(document);
        return parse(reader);
    }

    private static Node parse(HTMLStringReader reader) {
        String opening = reader.readTag();
        Node tag = creatNode(opening.substring(1, opening.length() - 1));
        if (tag.isSingle())
            return tag;

        while(true){
            if(reader.isUpcomingTag()){
                Node kid = parse(reader);
                if (kid.isSingle() && kid.getName().equals(tag.getName()))
                    return tag;
                tag.addChild(kid);
            }
            else
                tag.addText(reader.readString());
        }
    }

    private static Node creatNode(String tag) {
        boolean isClosing = false;
        int index = 0;
        if (tag.charAt(index) == HTMLStringReader.SLASH) {
            isClosing = true;
            index++;
        }
        if (Character.isWhitespace(tag.charAt(index)))
            throw  new RuntimeException("input String \"" + tag
                    + "\" is not a valid tag at index " + index);
        int endNameIndex = tag.indexOf(" ");
        if (endNameIndex == -1)
            endNameIndex = tag.length();
        String name = tag.substring(index, endNameIndex);
        if(!isValidTagName(name))
            throw new RuntimeException("input string \"" + tag
                    + "\" dose nothave a valid name");
        Node node = new Node(name, isClosing);
        if (endNameIndex == tag.length())
            return node;

        String[] attributes = tag.substring(endNameIndex + 1).split(" ");
        for (String attribute : attributes){
            if (attribute.isBlank())
                continue;
            String[] keyValue = attribute.split("=");
            if (keyValue[1].charAt(0) != '\"'
                    || keyValue[1].charAt(keyValue[1].length() - 1) != '\"')
                throw new RuntimeException("input string \"" + tag
                        + "\" dose not have a valid attribute : " + attribute);
            node.addAttribute(keyValue[0]
                    , keyValue[1].substring(1, keyValue[1].length() - 1));
        }
        return node;
    }

    private static boolean isValidTagName(String name) {
        return name.matches("[A-Za-z]+\\d*");
    }

    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        return root.toString();
    }
}
