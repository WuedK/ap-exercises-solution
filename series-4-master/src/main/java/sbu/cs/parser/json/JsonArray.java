package sbu.cs.parser.json;

import java.util.ArrayList;

public class JsonArray implements JsonElement{

	private ArrayList<JsonElement> list = new ArrayList<>();
	
	public void add(JsonElement element) {
		list.add(element);
	}
	
	public JsonElement get(int index) {
		return list.get(index);
	}
	
	public boolean update(int index, JsonElement element) {
		if (index < 0 || index >= list.size())
			return false;
		list.set(index, element);
		return true;
	}
	
	public boolean remove(int index) {
		if (index < 0 || index >= list.size())
			return false;
		list.remove(index);
		return true;
	}
	
	public int length() {
		return list.size();
	}
	
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public String toString() {
		return list.toString();
	}
	
}
