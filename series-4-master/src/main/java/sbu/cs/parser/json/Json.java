package sbu.cs.parser.json;

import sbu.cs.parser.helpers.Pair;

import java.util.ArrayList;

public class Json implements JsonInterface, JsonElement {
	
	private ArrayList<Pair<String, JsonElement>> dictionary = new ArrayList<>();
	
	public void add(String key, JsonElement value) {
		if (keyIndex(key) != -1)
			throw new KeyExistsException();
		dictionary.add(new Pair<String, JsonElement>(key, value));
	}
	
	public void update(String key, JsonElement value) {
		int index = keyIndex(key);
		if (index == -1)
			throw new KeyDoesNotExistsException();
		dictionary.get(index).setSecond(value);
	}
	
	public JsonElement get(String key) {
		int index = keyIndex(key);
		if (index == -1)
			throw new KeyDoesNotExistsException();
		return dictionary.get(index).getSecond();
	}
	
	public void remove(String key) {
		int index = keyIndex(key);
		if (index == -1)
			throw new KeyDoesNotExistsException();
		dictionary.remove(index);
	}
	
	private int keyIndex(String key) {
		int index = 0;
		for(Pair<String, JsonElement> pair : dictionary) {
			if (pair.getFirst().equals(key))
				return index;
			index++;
		}
		return -1;
	}
	
	public boolean isEmpty() {
		return dictionary.isEmpty();
	}
	
	public boolean contains(String key) {
		return keyIndex(key) != -1;
	}

    @Override
    public String getStringValue(String key) {
    	//return get(key).toString();

		//Changes for handling null values
		JsonElement value = get(key);
    	return value != null ? value.toString() : "null";
    }
    
//    { "name": 123, "name2": 345, }

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("{ ");
		for(Pair<String, JsonElement> pair : dictionary) {
			stringBuilder.append("\"");
			stringBuilder.append(pair.getFirst());
			stringBuilder.append("\": ");
			if (pair.getSecond() instanceof JsonString) {
				stringBuilder.append("\"");
				stringBuilder.append(pair.getSecond());
				stringBuilder.append("\"");
			}else {
				stringBuilder.append(pair.getSecond().toString());
			}
			stringBuilder.append(", ");
		}
		if (!isEmpty())
			stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "");
		stringBuilder.append("}");
		return stringBuilder.toString();
	}
        
}

