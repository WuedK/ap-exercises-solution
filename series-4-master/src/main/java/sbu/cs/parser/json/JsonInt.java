package sbu.cs.parser.json;

public class JsonInt implements JsonElement{

	private int value;

	public JsonInt(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(this.value);
	}
}
