package sbu.cs.parser.json;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        JsonStringReader reader = new JsonStringReader(data);
        return parseObject(reader);
    }
    
    private static Json parseObject(JsonStringReader reader) {
    	reader.readAculadOpen();
    	Json jsonObject = new Json();
    	while(reader.getUpComingChar() != JsonStringReader.ACULAD_CLOSE) {
    		String key = reader.readKey();
    		jsonObject.add(key, parseValue(reader));
    		if(reader.getUpComingChar() != JsonStringReader.ACULAD_CLOSE)
    			reader.readCama();
    	}
    	reader.readAculadClose();
    	return jsonObject;
    }
    
    private static JsonArray parseArray(JsonStringReader reader) {
    	reader.readBracketOpen();
    	JsonArray jsonArray = new JsonArray();
    	while(reader.getUpComingChar() != JsonStringReader.BRACKET_CLOSE) {
    		jsonArray.add(parseValue(reader));
    		if(reader.getUpComingChar() != JsonStringReader.BRACKET_CLOSE)
    			reader.readCama();
    	}
    	reader.readBracketClose();
    	return jsonArray;
    }
    
    private static JsonElement parseValue(JsonStringReader reader) {
    	JsonElement value;
    	char ch = reader.getUpComingChar();
    	switch (ch) {
	    	case JsonStringReader.DOUBLE_QUOTATION:
	    		value = new JsonString(reader.readString());
	    		break;
	    	case JsonStringReader.ACULAD_OPEN:
	    		value = parseObject(reader);
	    		break;
	    	case JsonStringReader.BRACKET_OPEN:
	    		value = parseArray(reader);
	    		break;
	    	case 'f':
	    	case 't':
	    		value = new JsonBool(reader.readBoolean());
	    		break;
	    	case 'n':
	    		value = reader.readNull();
	    		break;
	    	default:
	    		value = reader.isUpComingDouble() ? new JsonDouble(reader.readDouble())
	    				: new JsonInt(reader.readInt());
    	}
    	return value;
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return data.toString();
    }
}
