package sbu.cs.parser.json;

public class JsonString implements JsonElement{

	private String value;

	public JsonString(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		//return this.value;
		
		//Changes for handling quotation in quotation in Strings like: " \"salam\\\"salam\" "
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < value.length(); i++) {
			if (JsonStringReader.AFTER_DOUBLE_QUOTATION.contains("" + value.charAt(i)))
				stringBuilder.append('\\');
			stringBuilder.append(value.charAt(i));
		}
		return stringBuilder.toString();
	}
}
