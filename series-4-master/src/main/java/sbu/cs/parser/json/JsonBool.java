package sbu.cs.parser.json;

public class JsonBool implements JsonElement{

	private boolean value;

	public JsonBool(boolean value) {
		this.value = value;
	}

	public boolean getValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(this.value);
	}
}
