package sbu.cs.parser.json;

public class JsonStringReader {
	
	public static final char ACULAD_OPEN = '{';
	public static final char ACULAD_CLOSE = '}';
	public static final char DOUBLE_DOT = ':';
	public static final char CAMA = ',';
	public static final char DOUBLE_QUOTATION = '"';
	public static final char BRACKET_OPEN = '[';
	public static final char BRACKET_CLOSE = ']';
	public static final char BACKSLASH = '\\';

	public static final String SPECIAL_TOKEN = "{}:,\"[]";
	public static final String DOUBLE_NUMBER_TOKEN = "-0123456789.";
	public static final String INT_NUMBER_TOKEN = "-0123456789";
	public static final String AFTER_DOUBLE_QUOTATION = "\\\"\n\t";

	private String content;
	private int index;
	private boolean isInQuotation = false;
	
	public JsonStringReader(String content) {
		if (content == null)
			throw new IllegalArgumentException("json content can not be null");
		this.content = content;
	}

	private char read() {
		char ch;
		if(!isInQuotation) {
			while((ch = content.charAt(index)) == ' ' || ch == '\n' || ch == '\t')
				index++;
			if(isVaildCharacter(ch)) {
				if(index < content.length()-1)
					index++;
				char tmp;
				while(((tmp = content.charAt(index)) == ' ' || tmp == '\n' || tmp == '\t') 
						&& (index < content.length()-1))
					index++;
				return ch;
			}else {
				throwException();
			}
		}else {
			ch = content.charAt(index);
			index++;
			return ch;
		}
		
		return '\0';
	}
	
	private boolean isVaildCharacter(char ch) {
		return isInQuotation || SPECIAL_TOKEN.contains("" + ch) 
				|| DOUBLE_NUMBER_TOKEN.contains("" + ch);
	}
	
	private void throwException() {
		throw new  RuntimeException("Input string is not a valid JSON, error index: " + index);
	}
	
	public void readAculadOpen() {
        char ch = read();
        if (ch != ACULAD_OPEN) {
            throwException();
        }
    }

    public void readAculadClose() {
        char ch = read();
        if (ch != ACULAD_CLOSE) {
            throwException();
        }
    }

    public void readBracketOpen() {
        char ch = read();
        if (ch != BRACKET_OPEN) {
            throwException();
        }
    }

    public void readBracketClose() {
        char ch = read();
        if (ch != BRACKET_CLOSE) {
            throwException();
        }
    }
    
    public void readDoubleDot() {
        char ch = read();
        if (ch != DOUBLE_DOT) {
            throwException();
        }
    }

    public void readCama() {
        char ch = read();
        if (ch != CAMA) {
            throwException();
        }
    }
    
    public void readQuotation() {
        char ch = read();
        if (ch != DOUBLE_QUOTATION) {
            throwException();
        }
    }
    
    public char getUpComingChar() {
    	char ch;
    	int tmpIndex = index;
    	while((ch = content.charAt(index)) == ' ' || ch == '\n' || ch == '\t')
			index++;
    	return ch;
    }
    
    public int readInt() {
    	StringBuilder stringBuilder = new StringBuilder();
    	char ch;
    	while((ch = getUpComingChar()) != CAMA && ch != ACULAD_CLOSE && ch != BRACKET_CLOSE) {
    		read();
    		if (INT_NUMBER_TOKEN.contains("" + ch))
    			stringBuilder.append(ch);
    		else
    			throwException();
    	}
    	return Integer.parseInt(stringBuilder.toString());
    }
    
    public double readDouble() {
        StringBuilder builder = new StringBuilder();
        char ch;
        while ((ch = getUpComingChar()) != CAMA && ch != ACULAD_CLOSE && ch != BRACKET_CLOSE) {
            read();
            if (DOUBLE_NUMBER_TOKEN.contains("" + ch)) {
                builder.append(ch);
            } else {
                throwException();
            }
        }
        return Double.parseDouble(builder.toString());
    }

    public boolean readBoolean() {
        StringBuilder builder = new StringBuilder();
        isInQuotation = true;
        char ch;
        while ((ch = getUpComingChar()) != CAMA && ch != ACULAD_CLOSE && ch != BRACKET_CLOSE) {
        	read();
            builder.append(ch);
        }
        isInQuotation = false;
        return Boolean.parseBoolean(builder.toString());
    }
    
    public JsonElement readNull() {
    	StringBuilder builder = new StringBuilder();
        isInQuotation = true;
        char ch;
        while ((ch = getUpComingChar()) != CAMA && ch != ACULAD_CLOSE && ch != BRACKET_CLOSE) {
        	read();
            builder.append(ch);
        }
        isInQuotation = false;
        if(!builder.toString().equals("null"))
        	throwException();
        return null;
    }
    
    public String readString() {
        readQuotation();
        isInQuotation = true;
        StringBuilder builder = new StringBuilder();
        char ch = read();
        while (ch != DOUBLE_QUOTATION) {
            //builder.append(ch);
            //ch = read();
        	
    		//Changes for handling quotation in quotation in Strings like: " \"salam\\\"salam\" "
        	if (ch == BACKSLASH) {
        		ch = read();
        		if (!AFTER_DOUBLE_QUOTATION.contains("" + ch))
        			throwException();
        	}
        	builder.append(ch);
            ch = read();
        }
        String text = builder.toString();
        isInQuotation = false;
        return text;
    }
    
	public String readKey() {
        String key = readString();
        readDoubleDot();
        return key;
    }
	
	public boolean isUpComingDouble() {
		boolean isDotSeen = false;
		int backupIndex = index;
		char ch;
		while((ch = read()) != CAMA && ch != ACULAD_CLOSE && ch != BRACKET_CLOSE)
			isDotSeen = isDotSeen || ch == '.';
		index = backupIndex;
		return isDotSeen;
	}
}
