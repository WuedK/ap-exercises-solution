package sbu.cs.parser.json;

public class JsonDouble implements JsonElement{
	
	private double value;

	public JsonDouble(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(this.value);
	}
	
}
